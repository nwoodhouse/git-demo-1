<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = 
    [ 
    	'user_id',
    	'description',
    	'title'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function doThing()
    {
    	return 1 * 2;
    }
}
