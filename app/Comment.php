<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    // link it with the related user model
    public function user() 
    {
    	return $this->hasMany('App\User');
    }
}
