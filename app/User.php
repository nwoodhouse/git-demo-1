<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    // Ensure that the URL is included in this model's JSON
    protected $appends = [
        'url'
    ];


    // declare the 'url' as a dynamic attribute
    // get<nameOfAttribute>Attribute
    public function getUrlAttribute() {
        return route('users.show', $this->id);
    }

    public function posts()
    {
        return $this->hasMany('App\Post');
    }

    public function getPostsApiUrlAttribute()
    {
        return route('users.posts', $this->id);
    }
}
