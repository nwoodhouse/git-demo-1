<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    public function __construct($make)
    {
    	$this->make = $make;
    }
}
