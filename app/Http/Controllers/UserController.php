<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use App\Http\Requests;

// role of controle code, to:
// 1. Receive HTTP request
// 2. Decide which models to extract / manipulate
// 3. Decide what view to return, & pass models (or data) to that view to present
class UserController extends Controller
{

	// action (method in controller that handles 
	// HTTP request) to show a particular user profile
    public function show($id = -1)
    {
		$user = User::find($id);
		return view('demo', compact('user'));
    }

    public function superMagicAwesomeLookup(User $user)
    {
    	return view('demo', compact('user'));
    }


    // function to return the JSON representation of a given users posts
    public function posts(User $user)
    {
        return $user->posts;
    }
}
