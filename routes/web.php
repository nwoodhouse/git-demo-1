<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('user/{id?}', 'UserController@show')->name('users.show');



Route::get('magic/{user}', 'UserController@superMagicAwesomeLookup')->name('users.magic-show');


Route::get('nathans_page', function () {
	phpinfo();
	// return view('nathanspage');
})->name('nathan');

// Wire up all resource routes in one go!
// see: https://laravel.com/docs/5.3/controllers#resource-controllers
Route::resource('posts', 'PostController');


Route::get('api/v1/users/{user}/posts', 'UserController@posts')->name('users.posts');


















Route::get('/', function () {
	
    return view('welcome');
});








// route to display an index page of all cars!
Route::get('cars', function () {

	// retrieve the set of cars, that the view should show!
	$cars = 
	[
		new App\Car('Toyota'),
		new App\Car('Audi')
	];

	$title = "Car Shoowroom";

	// want all comments
	$comments = App\Comment::all();

	// return the view - controller decides the view!!!
	return view('cars', compact('cars', 'title', 'comments'));


});


Route::get('questions/{id}/{slug}', function ($name, $foo) {
	return 'Yoo ' . $name . $foo;
});

