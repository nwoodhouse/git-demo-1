<!-- a small, reusable bit of html to show some user data -->

<div class="user-info">
	<h3>Name: {{ $user->name }}</h3>
	<h3>Id: {{ $user->id }}</h3>
	<h3>Url: {{ $user->url }}</h3>
</div>