@extends('layouts.master')

@section('content')

	<form class="form-horizontal" action="{{ route('posts.store') }}" method="post">

		<!-- ensure that the form can pass CSRF protection -->
		<input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="form-group">
			<label for="title" class="col-sm-2 control-label">Title</label>
			<div class="col-sm-10">
			  <input name="title" type="text" class="form-control" id="title" placeholder="Title...">
			</div>
		</div>

		<div class="form-group">
			<label for="description" class="col-sm-2 control-label">Description</label>
			<div class="col-sm-10">
			  <input name="description" type="text" class="form-control" id="description" placeholder="Description">
			</div>
		</div>
	
		<div class="form-group">
			<div class="col-sm-10 col-sm-offset-2">
				<button class="btn btn-primary">Create!</button>
			</div>
		</div>

	</form>

@stop