@extends('layouts.master')

@section('content')

	<!-- Page content -->
	<h1>This page show user details</h1>
	<p>User details are important for users</p>

	@include('widgets.user-info')

	<div class="posts">
		
	</div>
@stop


@section('scripts')
	<script>
		// hit the users' post route and alert what i get back!
		var updatePosts = function () {

			// issue an AJAX request to the server, to load the posts in
			$.get('{{ $user->postsApiUrl }}', function (posts) {
				// here because the AJAX, background web request has completed!
				// posts is an array of posts!

				// display them!
				var $postsContainer = $('.posts');
				$postsContainer.empty();

				// loop over each post, and display it!
				for(var i=0; i<posts.length; i++) 
				{
					var currentPost = posts[i];

					// niave approach to client-side templating
					// you may want to investigate:
					// - handlebars.js
					// - vue.js
					// - angular.js
					// - look up client-side templating
					$postsContainer.append('<p>Title: ' + currentPost.title + '</p>');
				}
			});
		};

		// immediately update the posts so we don't have to wait 3 seconds...
		updatePosts();

		// cause the update to occur every 3000 seconds
		window.setInterval(updatePosts, 3000);

	</script>
@stop
